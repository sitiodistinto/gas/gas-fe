import './App.css';
import axios from "axios";
import Refuels from  './components/refuels';
import { useEffect, useState } from 'react';

const API_URL = "http://localhost:3000/api/v1/refuels";

function getAPIData() {
  return axios.get(API_URL).then((response) => response.data)
}

function App() {
  const [refuels, setRefuels] = useState([]);

  useEffect( ()=> {
    let mounted = true;
    getAPIData().then((items) => {
      if (mounted) {
        setRefuels(items);
      }
    });
    return () => { mounted = false };
  }, []);
  return (
    <div className='App'>
      <h1>Refuels</h1>
      <Refuels refuels={refuels} />
    </div>
  );
}

export default App;
