import React from 'react'

function Refuels(props) {
  return <div>
    <h1>Refuels from API</h1>
    { props.refuels.map((refuel) => {
      return (
        <div key={refuel.id}>
          <h2>{refuel.date}</h2>
          <p>{refuel.odometer}</p>
        </div>
      );
    })}
  </div>;
}

export default Refuels;